﻿namespace FestiveMall_Contract.Req
{
    public class GetstockReq
    {
        public int? CategoryId { get; set; }
        public string Searchtext { get; set; }
        public int? PageSize { get; set; }
        public int? PageNo { get; set; }
    }
}
