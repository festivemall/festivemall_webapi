﻿namespace FestiveMall_Contract.Req.Master
{
    public class AddColorReq
    {
        public string ColorName { get; set; }
    }
    public class AddSizeReq
    {
        public string SizeName { get; set; }
    }

    public class AddCategoryReq
    {
        public string CategoryName { get; set; }
    }

    public class AddUpdateProductReq
    {
        public int  CategoryId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        
    }
}