﻿namespace FestiveMall_Contract.Req.Master
{
    public class SendMailToAdminReq
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Reason { get; set; }
        public string Comment { get; set; }      
    }
}