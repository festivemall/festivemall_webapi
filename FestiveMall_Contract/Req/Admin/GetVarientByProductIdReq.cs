﻿namespace FestiveMall_Contract.Req.Admin
{
    public class GetVarientByProductIdReq
    {


        public string Searchtext { get; set; }
        public int? PageSize { get; set; }
        public int? PageNo { get; set; }
        public int? CategoryId { get; set; }
    }
}
