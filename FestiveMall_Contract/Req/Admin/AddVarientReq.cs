﻿using System.Collections.Generic;
namespace FestiveMall_Contract.Req.Admin
{
    public class AddVarientReq
    {
        public AddVarientReq()
        {
            Varient = new Varient();
            Stock = new Stock();
        }
        public Varient Varient { get; set; }
        public Stock Stock { get; set; }
    }
    public class Varient
    {
        public int? ProductId { get; set; }
        public int? ColourId { get; set; }
        public int? SizeId { get; set; }
        public int? UserId { get; set; }
    }
    public class Stock
    {
        public Stock()
        {
            ImageUrl = new List<ImageUrl>();            
        }
        public int? ProductVarientId { get; set; }
        public int? Quantity { get; set; }
        public decimal? Price { get; set; }
        public decimal? MRP { get; set; }
        public decimal? DiscountValue { get; set; }
        public int? UserId { get; set; }
        public List<ImageUrl> ImageUrl { get; set; }
    }
    public class ImageUrl
    {
        public int? StockImageDetailsId { get; set; }
        public string Url { get; set; }
        public string ActiveInd { get; set; }
    }
}