﻿namespace FestiveMall_Contract.Req.Login
{
    public class LoginReq
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}