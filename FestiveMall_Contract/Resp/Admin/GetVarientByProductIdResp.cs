﻿using System.Collections.Generic;
namespace FestiveMall_Contract.Resp.Admin
{
    public class GetVarientByProductIdResp
    {
        public GetVarientByProductIdResp()
        {
            rows = new List<Product>();
        }
        public List<Product> rows { get; set; }
        public int? count { get; set; }
    }
    public class Product
    {
        public Product()
        {
            Images = new List<Images>();
        }
        public string ProductName { get; set; }
        public int? ProductId { get; set; }

        public string CategoryName { get; set; }
        public int? CategoryId { get; set; }
        public int? ProductVarientId { get; set; }

        public decimal? MRP { get; set; }
        public decimal? DiscountValue { get; set; }


        public int? ColourId { get; set; }
        public string ColourName { get; set; }
        public int? SizeId { get; set; }
        public string Sizename { get; set; }
        public decimal? Price { get; set; }

        public List<Images> Images { get; set; }
    }
    public class Images
    {
        public string ImageUrl { get; set; }
        public int? StockImageDetailsId { get; set; }
    }
}