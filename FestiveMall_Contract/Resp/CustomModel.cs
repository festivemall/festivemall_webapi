﻿using System.Collections.Generic;

namespace FestiveMall_Contract.Resp
{
    public class CustomModel
    {
        public object data { get; set; }
        public string Message { get; set; }
        public int Status { get; set; }
        public List<string> Errors { get; set; }
    }
}
