﻿using System.Collections.Generic;
namespace FestiveMall_Contract.Resp
{
    public class GetstockResp
    {
        public GetstockResp()
        {
            rows = new List<StockProduct>();
        }
        public List<StockProduct> rows { get; set; }
        public int? count { get; set; }
    }
    public class StockProduct
    {
        public StockProduct()
        {
            Varients = new List<Varients>();
            Images = new List<StockImages>();
        }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Title { get; set; }
        public string ProductDescription { get; set; }
        public List<Varients> Varients { get; set; }
        public List<StockImages> Images { get; set; }
    }
    public class Varients
    {
        public int? Quantity { get; set; }
        public int? ProductVarientId { get; set; }
        public int? ColourId { get; set; }
        public string ColourName { get; set; }
        public int? SizeId { get; set; }
        public string Sizename { get; set; }
        public decimal? Price { get; set; }
        public decimal? MRP { get; set; }
        public decimal? DiscountValue { get; set; }



    }
    public class StockImages
    {
        public string ImageUrl { get; set; }
        public int? ProductVarientId { get; set; }
    }
}
