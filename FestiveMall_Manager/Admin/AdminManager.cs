﻿using FestiveMall_Common;
using FestiveMall_Contract.Req;
using FestiveMall_Contract.Req.Admin;
using FestiveMall_Contract.Resp;
using FestiveMall_DataAccess;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;
namespace FestiveMall_Manager.Admin
{
    public class AdminManager
    {
        CustomModel _customModel = new CustomModel();
        FestiveMallRepo repo = new FestiveMallRepo();

        public async Task<Proc_AddProductVarient_Result> AddProductVarient(Varient req)
        {
            using (var ent = new FestivemallEntities())
            {
                Proc_AddProductVarient_Result res = (await ent.Proc_AddProductVarient(req.ProductId, req.ColourId, req.SizeId, req.UserId).ToListAsync()).FirstOrDefault();
                return res;
            }
        }
        public async Task<Proc_Addstock_Result> AddStock(Stock req)
        {
            using (var ent = new FestivemallEntities())
            {
                string jsonreq = JsonConvert.SerializeObject(req);
                Proc_Addstock_Result res = (await ent.Proc_Addstock(jsonreq).ToListAsync()).FirstOrDefault();
                return res;
            }
        }


        public async Task<CustomModel> GetVarientByProductId(GetVarientByProductIdReq req)
        {
            _customModel.data = await repo.GetVarientByProductId(req);
            _customModel.Status = (int)OperationStatus.Success;
            return _customModel;
        }
        public async Task<CustomModel> Getstock(GetstockReq req)
        {
            _customModel.data = await repo.Getstock(req);
            _customModel.Status = (int)OperationStatus.Success;
            return _customModel;
        }
       

        public async Task<CustomModel> DeleteVarient(int? ProductVarientId)
        {
            _customModel.data = await repo.DeleteVarient(ProductVarientId);
            _customModel.Status = (int)OperationStatus.Success;
            return _customModel;
        }
     
    }
}
