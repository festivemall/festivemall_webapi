﻿using FestiveMall_Common;
using FestiveMall_Contract.Req.Master;
using FestiveMall_Contract.Resp;
using FestiveMall_DataAccess;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System.IO;
using System.Web.Hosting;

namespace FestiveMall_Manager.MasterData
{
    public class MasterDataManager
    {

        CustomModel _customModel = new CustomModel();
        public async Task<CustomModel> GetColour()
        {
            using (var ent = new FestivemallEntities())
            {
                _customModel.data = await ent.Proc_GetColour().ToListAsync();
                _customModel.Message = "Success";
                _customModel.Status = (int)OperationStatus.Success;
            }
            return _customModel;
        }


        public async Task<CustomModel> GetSizes()
        {
            using (var ent = new FestivemallEntities())
            {
                _customModel.data = await ent.Proc_GetSize().ToListAsync();
                _customModel.Message = "Success";
                _customModel.Status = (int)OperationStatus.Success;
            }
            return _customModel;
        }


        public async Task<CustomModel> GetProducts(int CategoryId, string SearchText, int PageSize, int PageNumber)
        {
            using (var ent = new FestivemallEntities())
            {
                _customModel.data = await ent.Proc_GetProducts(CategoryId, SearchText, PageSize, PageNumber).ToListAsync();
                _customModel.Message = "Success";
                _customModel.Status = (int)OperationStatus.Success;
            }
            return _customModel;
        }


        public async Task<CustomModel> GetCategories()
        {
            using (var ent = new FestivemallEntities())
            {
                _customModel.data = await ent.Proc_GetCategories().ToListAsync();
                _customModel.Message = "Success";
                _customModel.Status = (int)OperationStatus.Success;
            }
            return _customModel;
        }


        public async Task<CustomModel> AddColor(AddColorReq req)
        {
            using (var ent = new FestivemallEntities())
            {
                _customModel.data = (await ent.Proc_AddColour(req.ColorName).ToListAsync()).FirstOrDefault();
                _customModel.Status = (int)OperationStatus.Success;
                return _customModel;
            }
        }


        public async Task<CustomModel> AddCategory(AddCategoryReq req)
        {
            using (var ent = new FestivemallEntities())
            {
                _customModel.data = (await ent.Proc_AddCategory(req.CategoryName).ToListAsync()).FirstOrDefault();
                _customModel.Status = (int)OperationStatus.Success;
                return _customModel;
            }

        }

        public async Task<CustomModel> AddSize(AddSizeReq req)
        {
            using (var ent = new FestivemallEntities())
            {
                _customModel.data = (await ent.Proc_AddSize(req.SizeName).ToListAsync()).FirstOrDefault();
                _customModel.Status = (int)OperationStatus.Success;
                return _customModel;
            }

        }


        public async Task<CustomModel> AddUpdateProduct(AddUpdateProductReq req)
        {
            using (var ent = new FestivemallEntities())
            {
                _customModel.data = (await ent.proc_AddModifyProduct(req.CategoryId, req.ProductId, req.ProductName, req.ProductDescription).ToListAsync()).FirstOrDefault();
                _customModel.Status = (int)OperationStatus.Success;
                return _customModel;
            }
        }

        public async Task<CustomModel> SendMailToAdmin(SendMailToAdminReq req)
        {
            string mailbody = GenerateMailBody(req);
            _customModel.Message = await Email.SendEmail(mailbody);
            _customModel.Status = (int)OperationStatus.Success;
            return _customModel;
        }

        public string GenerateMailBody(SendMailToAdminReq req)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/MailTemplates/AdminMailBody.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{name}", req.Name);
            body = body.Replace("{mobile}", req.Mobile);
            body = body.Replace("{email}", req.Email);
            body = body.Replace("{reason}", req.Reason);
            return body;
        }
    }
}