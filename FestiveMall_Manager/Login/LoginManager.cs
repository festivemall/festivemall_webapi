﻿using FestiveMall_Common;
using FestiveMall_Contract.Req.Login;
using FestiveMall_Contract.Resp;
using FestiveMall_DataAccess;
using System.Threading.Tasks;
using System.Linq;
namespace FestiveMall_Manager.Login
{
    public class LoginManager
    {
        CustomModel _customModel = new CustomModel();
        public async Task<CustomModel> ValidateUserLogin(LoginReq req)
        {
            using (var ent = new FestivemallEntities())
            {                
                _customModel.data = (await ent.Proc_ValidateUserLogin(req.UserName, req.Password).ToListAsync()).FirstOrDefault();
                _customModel.Message = "Success";
                _customModel.Status = (int)OperationStatus.Success;
                return _customModel;
            }
        }
    }
}