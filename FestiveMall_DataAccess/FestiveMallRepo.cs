﻿using App.DBAccess;
using FestiveMall_Contract.Req;
using FestiveMall_Contract.Req.Admin;
using FestiveMall_Contract.Resp;
using FestiveMall_Contract.Resp.Admin;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
namespace FestiveMall_DataAccess
{
    public class FestiveMallRepo
    {
        DbHelper DbHelper = null;
        public FestiveMallRepo()
        {
            DbHelper = new DbHelper();
        }
        public async Task<GetVarientByProductIdResp> GetVarientByProductId(GetVarientByProductIdReq req)
        {
            GetVarientByProductIdResp oReturnModel = new GetVarientByProductIdResp();
            SqlParameter outParam = new SqlParameter("@p_response_string", SqlDbType.VarChar, -1);
            outParam.Direction = ParameterDirection.Output;
            SqlParameter[] procParameters = {
                new SqlParameter("@searchtext", req.Searchtext),
                new SqlParameter("@PageSize", req.PageSize),
                new SqlParameter("@PageNo", req.PageNo),
                new SqlParameter("@CategoryId", req.CategoryId),
                outParam
            };
            var result = await SqlDbHelper.GetJsonDataAsync<GetVarientByProductIdResp>("Proc_GetVarientByProductId", procParameters);
            string outValue = Convert.ToString(outParam.Value);
            return JsonConvert.DeserializeObject<GetVarientByProductIdResp>(outValue);
        }

        public async Task<GetstockResp> Getstock(GetstockReq req)
        {
            GetstockResp oReturnModel = new GetstockResp();
            SqlParameter outParam = new SqlParameter("@p_response_string", SqlDbType.VarChar, -1);
            outParam.Direction = ParameterDirection.Output;
            SqlParameter[] procParameters = {
                new SqlParameter("@CategoryId", req.CategoryId),
                new SqlParameter("@searchtext", req.Searchtext),
                new SqlParameter("@PageSize", req.PageSize),
                 new SqlParameter("@PageNo", req.PageNo),
                outParam
            };
            var result = await SqlDbHelper.GetJsonDataAsync<GetstockResp>("proc_Getstock", procParameters);
            string outValue = Convert.ToString(outParam.Value);
            return JsonConvert.DeserializeObject<GetstockResp>(outValue);
        }

        public async Task<DeleteVarientResp> DeleteVarient(int? ProductVarientId)
        {
            GetstockResp oReturnModel = new GetstockResp();
            SqlParameter[] procParameters = { new SqlParameter("@ProductVarientId", ProductVarientId) };
            return await SqlDbHelper.GetJsonDataAsync<DeleteVarientResp>("proc_DeleteVarient", procParameters);
        }
    }
}