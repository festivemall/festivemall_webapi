﻿using App.DBAccess;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace FestiveMall_DataAccess
{
    public class DbHelper
    {
        
        public async Task<IEnumerable<T>> GetDataAsync<T>(string queryString, SqlParameter[] sqlParams)
        {

            try
            {
                var result = await SqlDbHelper.GetDataAsync<T>(queryString, sqlParams);
                return result;

            }
            finally
            {

            }
        }
    }
}
