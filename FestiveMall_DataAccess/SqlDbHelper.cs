﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;


namespace App.DBAccess
{
    public class SqlDbHelper
    {
        private const string commandTimeOut = "DbCommandTimeOut";

        private static int CommandTimeOut
        {
            get
            {
                return ConfigurationManager.AppSettings["commandTimeOut"] != null ? Convert.ToInt16(ConfigurationManager.AppSettings["commandTimeOut"]) : 200;
            }
        }

        private static string ConnectionString
        {
            get
            {
                //return ConfigurationManager.AppSettings["SqlConnectionString"];

                return System.Configuration.ConfigurationManager.ConnectionStrings["FestivemallEntities1"].ConnectionString;
            }
        }

        /// <summary>
        /// This method will close and dispose the specified DbCommand object and its corresponding
        /// DbConnection.  This method should be called in the finally block of all queries to the database.
        /// </summary>
        /// <param name="cmd">The command object to close</param>
        protected static void CloseCommand(DbCommand cmd)
        {
            //close the connection
            cmd.Connection.Close();
            cmd.Connection.Dispose();
            cmd.Dispose();
        }

        /// <summary>
        /// This method will create a DbCommand object using the command text specified.
        /// </summary>
        /// <param name="commandText">The stored procedure name</param>
        /// <returns>A command object configured for stored procedures.</returns>
        public static DbCommand CreateCommand(string commandText)
        {
            return CreateCommand(commandText, CommandType.StoredProcedure);
        }

        protected static DbCommand CreateCommand(string commandText, CommandType type)
        {
            DbCommand cmd = DBFactory.CreateCommand();
            cmd.Connection = CreateConnection();
            cmd.CommandText = commandText;
            cmd.CommandType = type;
            return cmd;
        }

        /// <summary>
        /// This property is used to create ADO.Net objects for the specific provider.
        /// </summary>
        protected static DbProviderFactory DBFactory
        {
            get
            {
                if (_dbFactory == null)
                    _dbFactory = DbProviderFactories.GetFactory(ConnectionStringSettings.ProviderName);

                return _dbFactory;
            }
        }

        /// <summary>
        /// This method will create a connection object for the specific data base provider
        /// </summary>
        /// <returns></returns>
        protected static DbConnection CreateConnection()
        {
            DbConnection conn = DBFactory.CreateConnection();
            conn = new System.Data.SqlClient.SqlConnection();
            conn.ConnectionString = ConnectionStringSettings.ConnectionString;
            return conn;
        }

        #region Private Members

        private static System.Configuration.ConnectionStringSettings _connStr = null;
        private static DbProviderFactory _dbFactory = null;

        #endregion Private Members

        /// <summary>
        /// The ConnectionStringSettings object that is currently being used to connect these
        /// components to the database
        /// </summary>
        protected static ConnectionStringSettings ConnectionStringSettings
        {
            get
            {
                if (_connStr == null)
                {
                    //_connStr = ConfigurationManager.ConnectionStrings[connectionSetting];
                    if (_connStr == null) throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "Failed to create the connection string for: {0}", _connStr));
                }
                return _connStr;
            }
        }

        protected static DbParameter AddParameter(DbCommand cmd, string parameterName, DbType dbType, int size, object value)
        {
            DbParameter param = AddParameter(cmd, parameterName, dbType, value);
            param.Size = size;
            return param;
        }

        protected static DbParameter AddParameter(DbCommand cmd, string parameterName, DbType dbType, object value)
        {
            DbParameter param = cmd.CreateParameter();
            param.ParameterName = parameterName;
            param.DbType = dbType;
            param.Value = value;
            param.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(param);

            return param;
        }

        public static SqlParameter DbProcParameter(string parameterName, SqlDbType sqlDbType, ParameterDirection parameterDirection, bool isNullable, dynamic parameterVal)
        {

            SqlParameter pCountryId = new SqlParameter();
            pCountryId.ParameterName = parameterName;
            pCountryId.Direction = parameterDirection;
            pCountryId.SqlDbType = sqlDbType;
            pCountryId.IsNullable = isNullable;
            pCountryId.Value = parameterVal;
            return pCountryId;
        }

        public static IEnumerable<T> GetData<T>(string queryString, Func<IDataRecord, T> map)
        {
            var connection = new SqlConnection(ConnectionString);
            try
            {
                using (var command = new SqlCommand(queryString, connection))
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        return DataHelpers.DataMapperEntityMapper<T>(reader);
                    }
                }
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

        }

        public static IEnumerable<T> GetData<T>(string queryString)
        {
            var connection = new SqlConnection(ConnectionString);
            try
            {
                using (var command = new SqlCommand(queryString, connection))
                {
                    connection.Open();
                    command.CommandTimeout = CommandTimeOut;
                    using (var reader = command.ExecuteReader())
                    {
                        return DataHelpers.DataMapperEntityMapper<T>(reader);
                    }
                }
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
        }

        public static async Task<IEnumerable<T>> GetDataAsync<T>(string queryString, SqlParameter[] sqlParams)
        {
            var connection = new SqlConnection(ConnectionString);

            try
            {
                using (var command = new SqlCommand(queryString, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddRange(sqlParams);
                    command.CommandTimeout = CommandTimeOut;
                    connection.Open();

                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        return DataHelpers.DataMapperEntityMapper<T>(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
        }

        public static async Task<T> GetJsonDataAsync<T>(string queryString, SqlParameter[] sqlParams) where T : class
        {
            var connection = new SqlConnection(ConnectionString);

            try
            {
                using (var command = new SqlCommand(queryString, connection))
                {

                    foreach (var param in sqlParams)
                    {
                        if (param.Value == null)
                        {
                            param.Value = DBNull.Value;
                        }
                    }



                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddRange(sqlParams);
                    command.CommandTimeout = CommandTimeOut;
                    connection.Open();

                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        return DataHelpers.DataEntityMapper<T>(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
        }

        public static async Task<IEnumerable<T>> GetDataAsync<T>(string queryString)
        {
            var connection = new SqlConnection(ConnectionString);
            try
            {
                using (var command = new SqlCommand(queryString, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = CommandTimeOut;
                    connection.Open();

                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        return DataHelpers.DataMapperEntityMapper<T>(reader);
                    }
                }
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
        }

        public static async Task<int> SubmitData(string queryString, SqlParameter[] sqlParams)
        {
            var connection = new SqlConnection(ConnectionString);
            try
            {
                using (var command = new SqlCommand(queryString, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddRange(sqlParams);
                    command.CommandTimeout = CommandTimeOut;

                    connection.Open();

                    return await command.ExecuteNonQueryAsync().ConfigureAwait(false);
                }
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
        }
    }
}
