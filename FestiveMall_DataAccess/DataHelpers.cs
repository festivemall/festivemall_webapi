﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
 

namespace App.DBAccess
{
    public static class DataHelpers
    {

        public static List<T> GetList<T>(this IDataReader reader, Func<IDataRecord, T> mapTo)
        {
            var list = new List<T>();
            while (reader.Read())
                list.Add(mapTo(reader));
            return list;
        }

        public static List<T> DataMapperEntityMapper<T>(IDataReader dr)
        {
            List<T> list = new List<T>();
            T obj = default(T);
            var columns = Enumerable.Range(0, dr.FieldCount).Select(dr.GetName);
            if (typeof(T).Equals(typeof(string)) || typeof(T).Equals(typeof(int)))
            {
                while (dr.Read())
                    list.Add((T)dr[0]);
            }
            else
            {
                while (dr.Read())
                {
                    obj = Activator.CreateInstance<T>();
                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        var columnName = columns.Where(x => x.Replace("_", "").Replace(" ", "").Equals(prop.Name, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
                        if (columnName != null && !object.Equals(dr[columnName], DBNull.Value))
                        {
                            prop.SetValue(obj, dr[columnName], null);
                        }
                    }
                    list.Add(obj);
                }
            }
            return list;
        }

        public static T DataEntityMapper<T>(IDataReader dr) where T : class
        {
            List<T> list = new List<T>();
            T obj = default(T);
            var columns = Enumerable.Range(0, dr.FieldCount).Select(dr.GetName);
            if (typeof(T).Equals(typeof(string)) || typeof(T).Equals(typeof(int)))
            {
                while (dr.Read())
                    list.Add((T)dr[0]);
            }
            else
            {
                while (dr.Read())
                {
                    obj = Activator.CreateInstance<T>();
                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        var columnName = columns.Where(x => x.Replace("_", "").Replace(" ", "").Equals(prop.Name, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
                        if (columnName != null && !object.Equals(dr[columnName], DBNull.Value))
                        {
                            prop.SetValue(obj, dr[columnName], null);
                        }
                    }
                    list.Add(obj);
                }
            }

            if (list.Count > 0)
            {
                return list[0];
            }
            else
            {
                return null;
            }
        }

    }
}
