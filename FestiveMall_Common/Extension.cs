﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Threading.Tasks;

namespace FestiveMall_Common
{
    public static class Extension
    {
        public static async Task<List<T>> ToListAsync<T>(this ObjectResult<T> source)
        {
            var list = new List<T>();
            await Task.Run(() => list.AddRange(source.ToList()));
            return list;
        }
    }
}