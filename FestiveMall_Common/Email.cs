﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
namespace FestiveMall_Common
{
    public static class Email
    {
        public static async Task<string> SendEmail(string Body)
        {
            string message = string.Empty;
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(Common.address);
                mail.From = new MailAddress(Common.username, Common.name);
                MailAddress to = new MailAddress(Common.tomail, Common.toname);
                mail.To.Add(to);
                mail.Subject = Common.subject;
                mail.Body = Body;
                mail.IsBodyHtml = true;

                //System.Net.Mail.Attachment attachment;
                //attachment = new System.Net.Mail.Attachment("c:/textfile.txt");
                //mail.Attachments.Add(attachment);

                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Port = Common.port;
                SmtpServer.Credentials = new NetworkCredential(Common.username, Common.password);
                SmtpServer.EnableSsl = true;
                await SmtpServer.SendMailAsync(mail);
                message = null;
            }
            catch (Exception ex)
            {
                  message = ex.Message;
            }
            return message;
        }
    }
}