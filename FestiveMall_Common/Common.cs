﻿using System;
using System.Configuration;

namespace FestiveMall_Common
{
    public static class Common
    {
        public static string ProductImagePath = ConfigurationManager.AppSettings["ProductImagePath"].ToString();


        public static string name = ConfigurationManager.AppSettings["name"].ToString();
        public static string address = ConfigurationManager.AppSettings["address"].ToString();
        public static string username = ConfigurationManager.AppSettings["username"].ToString();
        public static string password = ConfigurationManager.AppSettings["password"].ToString();
        public static int port = Convert.ToInt32(ConfigurationManager.AppSettings["port"].ToString());
        public static string tomail = ConfigurationManager.AppSettings["tomail"].ToString();
        public static string toname = ConfigurationManager.AppSettings["toname"].ToString();
        public static string subject = ConfigurationManager.AppSettings["subject"].ToString();
        


    }
    public enum OperationStatus
    {
        Error = 0,
        Success = 1,
        LogicalError = 2
    }
}