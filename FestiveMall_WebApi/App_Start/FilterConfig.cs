﻿using FestiveMall_Common;
using FestiveMall_Contract.Resp;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Net;
namespace FestiveMall_WebApi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
    public class NotImplExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            //Send Error Response To Client
            CustomModel mod = new CustomModel
            {
                Status = (int)OperationStatus.Error,
                Message = context.Exception.Message.ToString()
            };
            var request = context.ActionContext.Request;
            context.Response = request.CreateResponse(HttpStatusCode.NotImplemented, mod);
        }
    }
}