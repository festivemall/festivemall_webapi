﻿using FestiveMall_Contract.Req.Master;
using FestiveMall_Contract.Resp;
using FestiveMall_Manager.MasterData;
using System.Threading.Tasks;
using System.Web.Http;
namespace FestiveMall_WebApi.Controllers.MasterData
{
    [RoutePrefix("api/MasterData")]
    public class MasterDataController : BaseApiController
    {
        MasterDataManager _masterDataManager = new MasterDataManager();      

        [HttpGet]
        [Route("GetColour")]
        public async Task<CustomModel> GetColour()
        {
            return await _masterDataManager.GetColour();
        }

        [HttpGet]
        [Route("GetSizes")]
        public async Task<CustomModel> GetSizes()
        {
            return await _masterDataManager.GetSizes();
        }

        [HttpGet]
        [Route("GetCategories")]
        public async Task<CustomModel> GetCategories()
        {
            return await _masterDataManager.GetCategories();
        }

       

        [HttpGet]
        [Route("GetProducts")]
        public async Task<CustomModel> GetProducts(int CategoryId,string SearchText,int PageSize,int PageNumber)
        {
            return await _masterDataManager.GetProducts(CategoryId, SearchText, PageSize, PageNumber);
        }



        [HttpPost]
        [Route("AddColor")]
        public async Task<CustomModel> AddColor([FromBody]AddColorReq req)
        {
            if (!ModelState.IsValid)
            {
                return await GetModelError(ModelState);
            }
            else
            {
                return await _masterDataManager.AddColor(req);
            }
        }

        [HttpPost]
        [Route("AddCategory")]
        public async Task<CustomModel> AddCategory([FromBody]AddCategoryReq req)
        {
            if (!ModelState.IsValid)
            {
                return await GetModelError(ModelState);
            }
            else
            {
                return await _masterDataManager.AddCategory(req);
            }
        }

        [HttpPost]
        [Route("AddSize")]
        public async Task<CustomModel> AddSize([FromBody]AddSizeReq req)
        {
            if (!ModelState.IsValid)
            {
                return await GetModelError(ModelState);
            }
            else
            {
                return await _masterDataManager.AddSize(req);
            }
        }

        

        [HttpPost]
        [Route("AddUpdateProduct")]
        public async Task<CustomModel> AddUpdateProduct([FromBody]AddUpdateProductReq req)
        {
            if (!ModelState.IsValid)
            {
                return await GetModelError(ModelState);
            }
            else
            {
                return await _masterDataManager.AddUpdateProduct(req);
            }
        }

        [HttpPost]
        [Route("SendMailToAdmin")]
        public async Task<CustomModel> SendMailToAdmin([FromBody]SendMailToAdminReq req)
        {
            if (!ModelState.IsValid)
            {
                return await GetModelError(ModelState);
            }
            else
            {
                return await _masterDataManager.SendMailToAdmin(req);
            }
        }

    }
}