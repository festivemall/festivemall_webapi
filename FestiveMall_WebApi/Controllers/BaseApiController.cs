﻿using FestiveMall_Common;
using FestiveMall_Contract.Resp;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
namespace FestiveMall_WebApi.Controllers
{
    public class BaseApiController : ApiController
    {
        public async Task<CustomModel> GetModelError(ModelStateDictionary model)
        {
            CustomModel obj = new CustomModel();
            obj.Errors = model.Values.SelectMany(x => x.Errors).Select(x => x.Exception.Message).ToList();
            obj.Message = "Invalid Request Object";
            obj.Status = (int)OperationStatus.LogicalError;
            return await Task.FromResult<CustomModel>(obj);
        }
    }
}
