﻿using FestiveMall_Contract.Req.Login;
using FestiveMall_Contract.Resp;
using FestiveMall_Manager.Login;
using System.Threading.Tasks;
using System.Web.Http;
namespace FestiveMall_WebApi.Controllers.Test
{
    [RoutePrefix("api/Login")]
    public class LoginController : BaseApiController
    {
        LoginManager _loginManager = new LoginManager();


        [HttpPost]
        [Route("ValidateUserLogin")]
        public async Task<CustomModel> ValidateUserLogin([FromBody]LoginReq req)
        {
            if (!ModelState.IsValid)
            {
                return await GetModelError(ModelState);
            }
            else
            {
                return await _loginManager.ValidateUserLogin(req);
            }
        }
    }
}
