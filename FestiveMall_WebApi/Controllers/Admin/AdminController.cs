﻿using FestiveMall_Common;
using FestiveMall_Contract.Req;
using FestiveMall_Contract.Req.Admin;
using FestiveMall_Contract.Resp;
using FestiveMall_Contract.Resp.Admin;
using FestiveMall_DataAccess;
using FestiveMall_Manager.Admin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Script.Serialization;
namespace FestiveMall_WebApi.Controllers.Admin
{
    [RoutePrefix("api/Admin")]
    public class AdminController : BaseApiController
    {
        AdminManager _adminManager = new AdminManager();
        CustomModel _customModel = new CustomModel();

        [HttpPost]
        [Route("AddVarient")]
        public async Task<CustomModel> AddVarient()
        {
            List<ImageUrl> TempImageUrl = new List<ImageUrl>();
            Proc_Addstock_Result AddStockResult = new Proc_Addstock_Result();


            var root = HttpContext.Current.Server.MapPath("~/App_Data/TempData");
            var provider = new MultipartFormDataStreamProvider(root);
            var result = await Request.Content.ReadAsMultipartAsync(provider);

            var model = result.FormData["model"];
            var AddVarientReq = new AddVarientReq();

            if (model != null)
            {
                //converting json to class model

                JavaScriptSerializer jss = new JavaScriptSerializer();
                jss.MaxJsonLength = Int32.MaxValue;
                AddVarientReq = jss.Deserialize<AddVarientReq>(model);
            }

            Proc_AddProductVarient_Result AddProductVarientResult = await _adminManager.AddProductVarient(AddVarientReq.Varient);
            AddVarientReq.Stock.ProductVarientId = AddProductVarientResult.ProductVarientId;


            if (AddProductVarientResult.ProductVarientId != 0)
            {
                //Delete all files from tempdata folder
                foreach (string file in Directory.GetFiles(root))
                {
                    File.Delete(file);
                }
                string sPath = HostingEnvironment.MapPath("~/Images/Varient/");
                HttpFileCollection hfc = HttpContext.Current.Request.Files;
                List<HttpPostedFile> list = Enumerable.Range(0, hfc.Count)
                                                      .Select(i => hfc[i]).ToList();
                if (list.Count > 0)
                {

                    foreach (HttpPostedFile hpf in list)
                    {
                        int indexOfFileType = hpf.FileName.Split('.').Length;
                        string filetype = hpf.FileName.Split('.')[indexOfFileType - 1];

                        int fileposition = Convert.ToInt32(hpf.FileName.Split('.')[0].Split('_')[1]);

                        string FileName = AddVarientReq.Varient.ProductId.ToString() + "_" + AddProductVarientResult.ProductVarientId.ToString() + "_" + fileposition.ToString() + "." + filetype;

                        hpf.SaveAs(sPath + Path.GetFileName(FileName));


                        TempImageUrl.Add(new ImageUrl()
                        {
                            Url = Common.ProductImagePath + FileName,
                            ActiveInd = "Y",
                            StockImageDetailsId = 0
                        });


                    }
                    AddVarientReq.Stock.ImageUrl = TempImageUrl;
                    AddStockResult = await _adminManager.AddStock(AddVarientReq.Stock);
                }
            }

            _customModel.data = AddStockResult;
            _customModel.Message = "Success";
            _customModel.Status = (int)OperationStatus.Success;
            return _customModel;
        }





        [HttpPost]
        [Route("DeleteVarient")]
        public async Task<CustomModel> DeleteVarient([FromBody]Product req)
        {
            CustomModel result = await _adminManager.DeleteVarient(req.ProductVarientId);

            string sPath = HostingEnvironment.MapPath("~/Images/Varient/");

            if (req.Images.Count > 0)
            {
                foreach (var img in req.Images)
                {
                    Uri uri = new Uri(img.ImageUrl);
                    string filename = Path.GetFileName(uri.LocalPath);
                    DeleteFiles(sPath, filename);
                }
            }
            _customModel.data = result.data;
            _customModel.Message = "Success";
            _customModel.Status = (int)OperationStatus.Success;
            return _customModel;
        }


        public string DeleteFiles(string Path, string FileName)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(Path);
            //string[] NamesArray = FileName.Split('.');
            //string partialName = NamesArray[0];
            FileInfo[] list = dirInfo.GetFiles("*" + FileName + "*.*");
            if (list.Count() > 0)
            {
                foreach (FileInfo fInfo in list)
                {
                    fInfo.Delete();
                }
            }
            return null;
        }



        [HttpPost]
        [Route("GetVarientByProductId")]
        public async Task<CustomModel> GetVarientByProductId([FromBody]GetVarientByProductIdReq req)
        {
            return await _adminManager.GetVarientByProductId(req);
        }

        [HttpPost]
        [Route("Getstock")]
        public async Task<CustomModel> Getstock([FromBody]GetstockReq req)
        {
            return await _adminManager.Getstock(req);
        }
    }
}